# NGDD Requirements Document

The document files are markdown, compiled with
[pandoc](https://pandoc.org/).

You can build the PDF manually with the following command:
```shell
$ pandoc --defaults pandoc.yml *-*.md -o ngdd-requirements.pdf
```
Or with the included Makefile:
```
$ make
```

An up-to-date copy of the PDF should also be available here: [NGDD Requirements
PDF](https://ngdd.docs.ligo.org/requirements-doc/ngdd-requirements.pdf)
