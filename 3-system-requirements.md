# System Requirements (SYS)

This set of requirements describe general requirements imposed on the system.

## Data Access (SYS-ACC)

**SYS-ACC-01:** Data access should be reliable, such that data
distribution/access should be able to receive data with some nominal latency
with some number of nines of reliability (TBD) outside of scheduled maintenance.

**SYS-ACC-02:** Relevant data-access services should be able to withstand node failures
without affecting service availability.

**SYS-ACC-03:** Data access services require access control for both read and write access
to the system, preferably via SciTokens.

**SYS-ACC-04:** Users require sub-second insert-to-query latency, denoting how soon data
can be accessed that is inserted into the system. Realistically, more than a few hundred
ms latency won’t be tolerable, and less than this would be ideal.

**SYS-ACC-05:** The system should use standard network protocols where appropriate.

**SYS-ACC-06:** Data should be distributed to production services that make use
of data without excessive network bandwidth. For example, for many consumers on
the same physical node accessing the same set of channels, deploying a
node-local server would avoid excessive bandwidth across the cluster's network.

**SYS-ACC-07:** The system shall support data retention policies set by management. This
is estimated to be O(PB) of archival data.

**SYS-ACC-08:** The system will need to track metadata associated with all timeseries stored
within the system, across all timespans stored within dictated by the retention policies
in SYS-ACC-07.

**SYS-ACC-09:** Failures from data access libraries should provide clear error handling.

**SYS-ACC-10:** Non-collaboration members and relevant stakeholders accessing released
data require public access to raw and derived data with access controls, as well as
unauthenticated public access from released data (open access) with restrictions on times
accessed.

**SYS-ACC-11:** Access to data should not be restricted to specific nodes to
allow for flexibility in regards to node failures, etc. Note: This is
specifically in regards to the current system for distributing low-latency raw
data which due to bandwidth limitations, can only be distributed to few nodes at
a given time. If one of the nodes fail, it is not easy for jobs to land on
another node to continue acquiring data.

## Data Publishing (SYS-PUB)

**SYS-PUB-01:** Low-latency production services must be able to publish sets of
derived channels into the system with minimal delay.

**SYS-PUB-02:** The system requires sufficient access control for controlling the
publishing of derived channel data into the system.

## Monitoring (SYS-MON)

**SYS-MON-01:** Users and service operations should be able to determine low-latency data
service status (e.g. is the service running and/or in a nominal state?) through a web user
interface to be informed of upstream issues.

**SYS-MON-02:** Users and service operators require sufficient monitoring and alerting of
the system to be deployed, to quickly diagnose or be informed of issues in the system.

**SYS-MON-03:** Users require a monitoring system for user applications which produce
derived data into the system.

## Application Orchestration (SYS-ORC)

**SYS-ORC-01:** The deployed system needs to be scalable, to handle data from
multiple observatories and many clients distributed across multiple data
centers. Ideally, it should be doable to scale up/down based on (static, but
also possibly dynamic) demand. This along with **SYS-ACC-06** implies that this
service needs to span across multiple data centers where raw/derived data is
generated as well as where many long-lived production consumers will be
located.

**SYS-ORC-02:** An orchestration system needs to be provided for managing services that
leverage the NGDD system, such as Nomad, Kubernetes or HTCondor, which handles
service dependencies, resource management (CPU/memory), and allocation of
resources in an opportunistic manner.

**SYS-ORC-03:** The orchestration system should be available for users to
deploy long-lived stream processing services.

## Other (SYS-ETC)

**SYS-ETC-01:** The user should only need the channels requested as well as the time
span to acquire data, i.e. any other details should be handled without any extra
input from the user.

**SYS-ETC-02:** Users should not need to know which server to connect to for
data/metadata, but instead can ask a default server for data, and that server
should route to the right place as needed. This encodes service and channel
discoverability.
