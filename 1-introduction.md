# Introduction

## Overview

LIGO is embarking on an effort to modernize, improve, and extend its low-latency
data delivery and stream processing systems. LIGO is acknowledging the
importance of the low-latency search/alert pipelines to our overall science
goals, and is making a significant investment in the components of the system it
is responsible for. This document intends to define the requirements for this
new system.

![](data_flow.png)

The low-latency delivery and stream processing system is concerned with providing access
to and distributing continuous streams of timeseries data, which can be categorically
split into two groups; raw and derived data streams. The raw data streams describe data
which is acquired from the detector’s data acquisition (DAQ) system, while derived data
streams are produced from stream processing applications downstream of the DAQ, and
generally take as input one or more raw data streams. Examples of derived channels include
timeseries generated from calibration, data quality and noise subtraction services.

Access locations include intra-site computing clusters (co-located where the
interferometer is), inter-site computing clusters (such as the Caltech computing
cluster), as well as general authenticated remote access.

## Stakeholders

Stakeholders within the LIGO collaboration include:

* Consumers of raw and/or derived channels, including, but not limited to:
  * Calibration
  * Commissioners
  * Detector Characterization
  * GW searches
  * Parameter Estimation
* Producers of derived channels
  * Calibration
  * Detector Characterization
  * GW searches

## Access Patterns

Users access data in a variety of different ways, but we can classify these
access patterns into a number of classes in the following table:

| User Group  | # Channels | # Consumers | Access Location | Channels           |
| :---------- | :--------- | :---------- | :-------------- | :----------------- |
| DetChar     | O(1000)    | O(10)       | Cluster         | "DetChar"          |
| GW Searches | O(10)      | O(1000)     | Cluster         | "hoft"             |
| PE          | O(10)      | O(10)       | Cluster         | "hoft", SVD coeffs |
| Other       | O(100)     | O(100)      | Anywhere        | Any                |

## Goals

We want to build an entirely new system from scratch, based on modern
industry-proven stream processing technologies to provide a low-latency data
distribution and stream processing platform, meeting the needs of real-time
searches for gravitational waves.

Desired features:

* Easy creation of stream processing elements.
* Simple to subscribe to any raw or derived data channel from anywhere in the
  cluster.
* Applications can easily produce new derived data channels that are immediately
  available to any other application in the system.
* Archive of derived channels as first class data products along with raw
  channels.
* Offline playback of both raw and derived channels.

## Scope

The low-latency delivery and stream processing system is focused on handling and
serving continuous time series data, i.e. data with a well-defined sampling rate
as well as any metadata associated with time series data, but does not include
asynchronous event streams or other gravitational-wave data products generated
by downstream services.
