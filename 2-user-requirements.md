# User Requirements (USR)

Users refer to any one of the stakeholders listed above and imposes requirements on the
low-latency data access and delivery system.

**USR-01:** Users and developers require idiomatic libraries for accessing and publishing
data for Python, C++ and Rust.

**USR-02:** Users can request any combination of channels without restriction, except
possibly on the number of channels to request at once.

**USR-03:** Users require access to raw channel data, derived channel data, trend data,
and state vector information.

**USR-04:** Users require synchronization of delivered channels such that data with the
same start GPS time are grouped together into a single chunk.

**USR-05:** Users need at minimum, a user guide and API documentation for any
client-facing libraries that’s easily discoverable.

**USR-06:** Configurable gap handling is required for data access libraries, i.e. how
should the client handle missing data for data that is requested.

**USR-07:** Users should be able to request low-latency data with a specific GPS timestamp
so they can ‘pick back up where they left off’ in the case of failures, etc. via a
lookback/seeking mechanism.

**USR-08:** Low-latency data must be made available with minimal (<1 second) delay in
nominal operation.

**USR-09:** Data access needs to be made reliable for production services.

**USR-10:** h(t) and related channel data used in searches shall be made available
generally on non-site clusters where processing is commonly done.

**USR-11:** The API for relevant data access libraries should support something akin to a
tail/listen to return data as it becomes available for live data.

**USR-12:** Data access libraries need to support a unified method to access live and
historical data, such that users need not be concerned about where to access data depending
on the GPS time requested.

**USR-13:** Data access libraries should be able to handle intrinsic delays for sets of
channels produced at different latencies gracefully, such that data is returned as soon as
it is available but also able to time out for channels that don’t have data within some
latency tolerance.

**USR-14:** Users should be able to determine low-latency data service status (e.g. is the
service running and/or in a nominal state?) through a web user interface to be informed of
upstream issues.
