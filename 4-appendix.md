# Appendix

## Acronyms

_DAQ_
: Data Acquisition

_DetChar_
: Detector Characterization

_GW_
: Gravitational Wave

_IFO_
: Interferometer

_NGDD_
: Next Generation Data Delivery, the interim name for this project

_PE_
: Parameter Estimation

_SVD_
: Singular Value Decomposition

## Definition of Terms

_low-latency_
: Times immediately after real time (~seconds)
